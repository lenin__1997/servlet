

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public HelloServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8;");
		PrintWriter out = response.getWriter();
		String uname = request.getParameter("name");
		String upassword = request.getParameter("password");
		
		if(uname.equals("lenin") && upassword.equals("1234")) {
			out.println("<h3 align=center> Hello "+ uname +" It's nice to see you.");
		}
		else {
			out.println("Something went Wrong.");
		}
		out.close();
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out1 = response.getWriter();
		String uname1 = request.getParameter("name1");
		
			out1.println("<h3 align=center>Hello "+ uname1 +" Printed</h3>");
	}
		
	}


