<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Title</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

</head>

<body>
  <form method="get" action="HelloServlet"  class="d-flex justify-content-center m-3">
  <div class="card col-4 " >
      <div class="card-body  ">
		  <div class="col-4 m-4 form-group" style="width:90%;">
		    <input class="form-control " type="text" name="name" placeholder="Enter Your name">
		  </div>
		  <div class="col-4 m-4 form-group" style="width:90%;">
		    <input class="form-control " type="password" name="password" placeholder="Enter Your name Password">
		  </div>
		  <div class="col-4 m-4 form-group d-flex justify-content-around" style="width:90%;">
		      <input type="submit" class="btn btn-primary" value="post">
		    <input type="reset" class="btn btn-primary">
		  </div>      	
    </div>
  </div>
    </form>
  
  <form method="post" action="HelloServlet" class="d-flex justify-content-center m-3">
  <div class="card col-4 " >
      <div class="card-body  ">
	  <div class="col-4 m-4 form-group" style="width:90%;">
	    <input class="form-control " type="text" name="name1" placeholder="Enter Your name">
	  </div>
	
	  <div class="col-4 m-4 form-group d-flex justify-content-center" >
	      <input type="submit" class="btn btn-primary" value="get" >
	  </div>
    </div>
  </div>
</form>
  
  
  
  
     <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
</body>

</html>